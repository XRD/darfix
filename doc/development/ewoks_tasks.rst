Ewoks Tasks
===========

Each Darfix widget has an underlying Ewoks task that takes care of the processing. The Ewoks tasks can be used independently for headless processing.

The documentation for these Ewoks tasks is given below.

.. ewokstasks:: darfix.tasks.*
    :task_type: class
