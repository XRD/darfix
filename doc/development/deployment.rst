Deployment at the ESRF
======================

Deployment is done with `darfix apptainer <https://gitlab.esrf.fr/apptainer/darfix>`_
See details in `README.md <https://gitlab.esrf.fr/apptainer/darfix/-/blob/main/README.md>`_
