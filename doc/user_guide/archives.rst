Archives
--------


Tutorial darfix 1.0

.. raw:: html
    
    <div style='display: flex; flex-direction: column'>
        <object height='400' type="application/pdf" data="../_static/darfix_guide.pdf">
            <p>The PDF cannot be displayed. Please download it using the link below.</p>
        </object>
    <div>

.. note::

    The PDF is available for download `here <../_static/darfix_guide.pdf>`_.
