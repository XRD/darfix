Widgets
-------

.. toctree::
   :maxdepth: 1

   roiselection
   edfdataselection
   hdf5dataselection
   hdf5concatenation
   shiftcorrection
   noisereduction
   datacopy
   blindsourceseparation
   datapartition
   dimensions
