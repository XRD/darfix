HDF5 data selection
===================

Data selection dedicated to HDF5 datasets (like bliss datasets).

.. image:: icons/upload_hdf5.svg

Select the raw data and dark frames from disk.

.. hint::

   for edf dataset see :ref:`EDF data selection`

Signals
-------

**Outputs**:

- Dataset: output dataset

**Inputs**:

- raw input file: file containing the raw data
- dark input file: *optional* file containing the dark data (background)

Description
-----------

**Data** widget retrieves selected files from disk, loads the data in a darfix dataset and sends it to the output.
The files are stored in the local memory and the necessary information for the analysis
is retrieved.

1. If the raw data is read only, the treated data path should be filled with the
   path of a directory with write access.
2. If the data doesn't fit in memory, the *in disk* checkbox should be ticked.


Demo
----

.. video:: http://www.silx.org/pub/doc/darfix/video/data_input/load_hdf5_dataset.webm
   :width: 500


Tutorial
--------


1. Add the widget to the canvas
'''''''''''''''''''''''''''''''

.. image:: img/hdf5_data_selection/add_widget.png
   :width: 500 px

2. Open the widget interface
''''''''''''''''''''''''''''

.. image:: img/hdf5_data_selection/startup_interface.png
   :width: 500 px

3. Select the file containing raw data
''''''''''''''''''''''''''''''''''''''

.. image:: img/hdf5_data_selection/select_raw_data.png
   :width: 500 px

.. note::

   darfix will retrieve from the file the detector and the motor positions (positioners) used for fitting dimensions. By default, it expects:

   * the path to the detector to be `{scan}/measurement/{detector}/data`
   * the path to positioners to be `{scan}/instruments/positioners`.

   'advanced' mode allows to change the path to the detector and the positioner group in the case those are
   not stored at default location of if the file contains more than one scan.
   The advanced mode gives indications on the validity of the data path:

   * green dot: the pattern can be solved (with the current input file)
   * red dot: the pattern cannot be solved (with the current input file).
   * question mark: no file has been given. Select a file before specifying a pattern.

.. image:: img/hdf5_data_selection/select_raw_data_advanced.png
   :width: 500 px

4. (optional) provide the file containing the dark data
'''''''''''''''''''''''''''''''''''''''''''''''''''''''

.. image:: img/hdf5_data_selection/dark_data_file.png
   :width: 500 px

5. (optional) provide the treated data directory
''''''''''''''''''''''''''''''''''''''''''''''''

.. image:: img/hdf5_data_selection/treated_data_dir.png
   :width: 500 px

6. Validate
'''''''''''

click on the 'Ok' button. This will load the datasets and trigger downstream widgets (if any)
