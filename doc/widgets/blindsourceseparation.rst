blind source separation
=======================

.. image:: icons/bss.png

Widget to apply blind source separation (BSS) to find grains along the dataset.
Several techniques can be used like NMF, NNICA, and NMF+NNICA.

Signals
-------

- Dataset

**Outputs**:

- Dataset

Description
-----------

TODO