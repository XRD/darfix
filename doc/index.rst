Darfix
======

The purpose of `darfix` is to create a set of functions and classes
to help ESRF beamline ID06-HXRM read a series of data images and apply
the operations needed for the experiments.

.. toctree::
   :hidden:

   tutorials/index.rst
   user_guide/index.rst
   widgets/index.rst
   development/index.rst

.. toctree::
   :maxdepth: 1

   changelog.rst
   license.rst
