License
=======

The source code of *darfix* is licensed under the `MIT <https://opensource.org/licenses/MIT>`_ license:

.. include:: ../LICENSE
