Getting started
===============

Launch the Orange canvas

.. code-block:: bash

    darfix

Then, you can have a look at the example workflows in `Help > Example workflows`.

The :doc:`../user_guide` and the :doc:`../widgets/index` documentation give detailed explanations on the different tasks in the example workflows.

If you already have a workflow, you can open it directly in the Orange canvas using

.. code-block:: bash

    darfix my_workflow.ows
