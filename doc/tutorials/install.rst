Installation
============

Installing *darfix* involves creating a python environment, installing dependencies and potentially
Windows enabling long paths.

Environment
-----------

It is recommended to create a `virtual environment <https://docs.python.org/3/library/venv.html>`_ to
avoid conflicts between dependencies.

On Linux or Mac

.. code-block:: bash

    python3 -m venv /path/to/new/virtual/environment

    source /path/to/new/virtual/environment/bin/activate

On Windows

.. code-block:: bash

    python3 -m venv C:\path\to\new\virtual\environment

    C:\path\to\new\virtual\environment\Scripts\activate.bat

*Note: To deactivate the environment, call:* :code:`deactivate`

Install
-------

Installation from pypi
^^^^^^^^^^^^^^^^^^^^^^

Recommended installation (with all *darfix* dependencies) is:

.. code-block:: bash

    pip install darfix[full]


.. note::

    To install *darfix* with a minimal set of dependencies you can run instead

    .. code-block:: bash

        pip install darfix

.. hint::

    If you have trouble during installation see the :ref:`Troubleshooting <Troubleshooting>` section


Install from sources
^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

    git clone https://gitlab.esrf.fr/XRD/darfix.git
    cd darfix
    pip install .[full]


.. note::

    Or with a minimal set of dependencies:

    .. code-block:: bash

        pip install .

.. hint::

    If you have trouble during installation see the :ref:`Troubleshooting <Troubleshooting>` section


Test
^^^^

To test the orange workflow (only if you have access to the source code)

.. code-block:: bash

    darfix src/orangecontrib/darfix/tutorials/darfix_example_hdf.ows



Troubleshooting
^^^^^^^^^^^^^^^

On Windows you may get this installation error

.. code-block:: bash

    Building wheels for collected packages: ewoksorange
    Building wheel for ewoksorange (pyproject.toml) ... error
    error: subprocess-exited-with-error

    × Building wheel for ewoksorange (pyproject.toml) did not run successfully.
    │ exit code: 1
    ╰─> [154 lines of output]
        ...
        error: could not create 'build\lib\ewoksorange\tests\examples\ewoks_example_1_addon\orangecontrib\ewoks_example_supercategory\ewoks_example_subcategory\tutorials\sumtask_tutorial3.ows': No such file or directory
        [end of output]

    note: This error originates from a subprocess, and is likely not a problem with pip.
    ERROR: Failed building wheel for ewoksorange
    Failed to build ewoksorange
    ERROR: Could not build wheels for ewoksorange, which is required to install pyproject.toml-based projects

If you do, you need to enable long paths. Instructions to do this in Windows 10 or later can be found `here <https://learn.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation?tabs=registry>`_.


Use
---

Start the graphical interface

.. code-block:: bash

    darfix

Drag widgets from the left panel to the canvas to start making your workflow.

