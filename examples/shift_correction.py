import numpy
from silx.gui import qt

from darfix.core.dimension import POSITIONER_METADATA
from darfix.dtypes import Dataset
from darfix.tests.utils import create_3motors_dataset
from darfix.tests.utils import createDataset
from orangecontrib.darfix.widgets.shiftcorrection import ShiftCorrectionWidgetOW

USE_3D_DATASET = False


if __name__ == "__main__":
    qapp = qt.QApplication([])

    w = ShiftCorrectionWidgetOW()

    if USE_3D_DATASET:
        dataset = create_3motors_dataset(dir=None, in_memory=True, backend="hdf5")
        dataset.find_dimensions(POSITIONER_METADATA)
        dataset = dataset.reshape_data()
    else:
        data = numpy.zeros(30000).reshape((10, 50, 60))
        for i in range(10):
            data[i, 3 * i + 10 : 3 * i + 20, 2 * i + 10 : 2 * i + 20] = 1

        dataset = createDataset(data)
        dataset.find_dimensions(POSITIONER_METADATA)

    w.setDataset(Dataset(dataset))
    w.show()

    qapp.exec()
