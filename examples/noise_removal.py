import argparse
import sys

import numpy
from silx.gui import qt

from darfix import dtypes
from darfix.tests.utils import createDataset
from orangecontrib.darfix.widgets.noiseremoval import NoiseRemovalWidgetOW


def main(argv=None):
    if argv is None:
        argv = sys.argv
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--edf-dataset",
        help="Create the example from an EDF random dataset. Else from an HDF5 one",
        default=False,
        action="store_true",
    )

    options = parser.parse_args(argv[1:])

    qapp = qt.QApplication([])

    w = NoiseRemovalWidgetOW()

    # test data
    data = numpy.arange(2500).reshape((1, 50, 50))
    dark_frames = data.copy()
    data[:, 10:30, 10:20] = 2000

    data = numpy.repeat(data, 10, axis=0)

    if options.edf_dataset:
        backend = "edf"
    else:
        backend = "hdf5"
    dataset = createDataset(data=data, backend=backend)
    bg_dataset = createDataset(data=dark_frames, backend=backend)
    w.setDataset(dtypes.Dataset(dataset=dataset, bg_dataset=bg_dataset))
    w.show()

    qapp.exec()


if __name__ == "__main__":
    main(sys.argv)
