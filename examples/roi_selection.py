"""Example showing the widget :mod:`~darfix.gui.roiSelectionWidget.ROISelectionWidget`."""

__authors__ = ["J. Garriga"]
__license__ = "MIT"
__date__ = "06/12/2019"


import argparse
import signal
import sys

import numpy
from silx.gui import qt

from darfix import dtypes
from darfix.gui.roiSelectionWidget import ROISelectionWidget
from darfix.tests.utils import createDataset


def exec_(argv=None):
    if argv is None:
        argv = sys.argv
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--edf-dataset",
        help="Create the example from an EDF random dataset. Else from an HDF5 one",
        default=False,
        action="store_true",
    )
    options = parser.parse_args(argv[1:])

    qapp = qt.QApplication([])

    # add connection with ctrl + c signal
    qt.QLocale.setDefault(qt.QLocale.c())
    signal.signal(signal.SIGINT, sigintHandler)
    sys.excepthook = qt.exceptionHandler
    timer = qt.QTimer()
    timer.start(500)
    # Application have to wake up Python interpreter, else SIGINT is not
    # catch
    timer.timeout.connect(lambda: None)

    w = ROISelectionWidget()

    # test data
    data = numpy.arange(2500).reshape((1, 50, 50))
    data[:, 10:30, 10:20] = 0

    data = numpy.repeat(data, 10, axis=0)

    if options.edf_dataset:
        backend = "edf"
    else:
        backend = "hdf5"
    dataset = createDataset(data=data, backend=backend)

    w.setDataset(dtypes.Dataset(dataset=dataset))
    w.show()

    qapp.exec()


def sigintHandler(*args):
    """Handler for the SIGINT signal."""
    qt.QApplication.quit()


if __name__ == "__main__":
    exec_(sys.argv)
