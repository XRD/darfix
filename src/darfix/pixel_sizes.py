from silx.utils.enum import Enum as _Enum


class PixelSize(_Enum):
    """
    Different pixel sizes
    """

    Basler = 0.051
    PcoEdge_2x = 0.00375
    PcoEdge_10x = 0.00075
