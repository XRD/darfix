from typing import Tuple

SCALE_FACTOR = 100

Vector3D = Tuple[float, float, float]
