# darfix

_darfix_ is a Python library for the analysis of dark-field microscopy data. It provides a series of computer vision techniques,
together with a graphical user interface and an [Orange3](https://github.com/biolab/orange3) add-on to define the workflow.

## Getting started

```bash
pip install darfix[full]
```

Then, launch the Orange canvas with

```bash
darfix
```

Drag widgets from the left panel to the canvas to start making your workflow.

## Documentation

The documentation of the latest release is available at https://darfix.readthedocs.io

## User guide

A user guide can be downloaded at https://darfix.readthedocs.io/en/latest/user_guide.html
